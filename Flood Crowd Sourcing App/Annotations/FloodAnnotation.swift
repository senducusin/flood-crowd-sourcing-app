//
//  FloodAnnotation.swift
//  Flood Crowd Sourcing App
//
//  Created by Jansen Ducusin on 2/16/21.
//

import Foundation
import MapKit
import UIKit

class FloodAnnotation: MKPointAnnotation {
    let flood:  Flood
    
    init(_ flood:   Flood){
        self.flood = flood
    }
    
    
}
