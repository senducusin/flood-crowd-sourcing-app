//
//  Date+Extensions.swift
//  Flood Crowd Sourcing App
//
//  Created by Jansen Ducusin on 2/15/21.
//

import Foundation

extension Date {
    func formatAsString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: self)
    }
}
