//
//  MapViewController.swift
//  Flood Crowd Sourcing App
//
//  Created by Jansen Ducusin on 2/15/21.
//

import Foundation
import UIKit
import MapKit
import CoreLocation
import FirebaseFirestore

class MapViewController: UIViewController{
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var reportFloodButton:   UIButton!
    
    private var documentRef: DocumentReference!
    
    private(set) var floods = [Flood]()
    
    private let collectionName = "flooded-regions"
    private let floodAnnotationIdentifer = "FloodAnnotationView"
    
    private lazy var locationManager:   CLLocationManager = {
        let manager = CLLocationManager()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.distanceFilter = kCLHeadingFilterNone
        manager.requestAlwaysAuthorization()
        
        return manager
    }()
    
    private lazy var db:    Firestore = {
        let firestoreDB = Firestore.firestore()
        return firestoreDB
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupMapLocation()
        setupUI()
        setupObservers()
    }
    
    @IBAction func reportAreaAsFlooded(){
        saveReportedFloodToFirebase()
    }
    
    private func addFloodToMap(_ flood: Flood){
        let annotation = FloodAnnotation(flood)
        annotation.coordinate = CLLocationCoordinate2D(latitude: flood.latitude, longitude: flood.longitude)
        annotation.title = "Flooded!"
        annotation.subtitle = flood.reportedDate.formatAsString()
        
        self.mapView.addAnnotation(annotation)
    }
    
    private func saveReportedFloodToFirebase(){
        guard let location = self.locationManager.location else{
            print("Location is not found")
            return
        }
        
        var flood = Flood(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        self.documentRef = self.db.collection(collectionName).addDocument(data: flood.toDictionary()) { [weak self] error in
            if let error = error {
                print(error)
            }else{
                flood.documentId = self?.documentRef.documentID
                self?.addFloodToMap(flood)
            }
        }
        
    }
    
    private func updateAnnotations(){
        DispatchQueue.main.async {
            self.mapView.removeAnnotations((self.mapView.annotations))
            self.floods.forEach{
                self.addFloodToMap($0)
            }
        }
    }
    
    private func setupObservers(){
        self.db.collection(collectionName).addSnapshotListener(){ [weak self] snapshot, error in
            
            guard let snapshot = snapshot,
                  error == nil else {
                print("Error fetching from Firestore")
                return
            }
            
            snapshot.documentChanges.forEach{ diff in
                if diff.type == .added {
                    if let flood = Flood(diff.document){
                        self?.floods.append(flood)
                        self?.updateAnnotations()
                    }
                }else if diff.type == .removed {
                    if let flood = Flood(diff.document){
                        if let floods = self?.floods {
                            self?.floods = floods.filter{$0.documentId != flood.documentId}
                            self?.updateAnnotations()
                        }
                    }
                }
            }
        }
    }
    
    private func setupMapLocation(){
        self.locationManager.startUpdatingLocation()
        self.mapView.showsUserLocation = true
        self.mapView.delegate = self
    }
    
    private func setupUI(){
        self.reportFloodButton.layer.cornerRadius = 10.0
        self.reportFloodButton.layer.masksToBounds = true
    }
    
}

extension MapViewController: MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        let region = MKCoordinateRegion(center: self.mapView.userLocation.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.08, longitudeDelta: 0.08))
        
        self.mapView.setRegion(region, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: self.floodAnnotationIdentifer)
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: floodAnnotationIdentifer)
            annotationView?.canShowCallout = true
            annotationView?.image = UIImage(named: "Annotation.png")
            annotationView?.rightCalloutAccessoryView = UIButton.buttonForRightAccessoryView()
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        if let floodAnnotation = view.annotation as? FloodAnnotation {
            let flood = floodAnnotation.flood
            
            self.db.collection(collectionName).document(flood.documentId!).delete(){ [weak self] error in
                if let error = error {
                    print("Error removing document from collection.\n\(error)")
                }else {
                    DispatchQueue.main.async {
                        self?.updateAnnotations()
                    }
                }
            }
        }
    }
}
