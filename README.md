**Flood Crowd Sourcing App using MVC**

A crowd sourcing application that aims to contribute in the community by displaying areas which are flagged as affected by flood by other users. 

This is a sample app that utilizes Firebase's Firestore, MVC, MapKit, CoreLocation, and UIKit. The source code, demonstrates a simple implementation of MVC structure in Swift.
  
---

## API

The communication between the app and Firebase database is provided by the Firebase API and framework.

---

## Features

- Has a realtime map user interface  
- Updates in realtime  
- Flag current location as affected by flood  
- Remove flagged locations when the location is no longer affected by flood

---

## How to compile using Xcode
1. Clone this repo  
2. Open a terminal in the project's directory  
3. Run 'pod install', wait for it to finish  
4. Open 'Flood Crowd Sourcing App.xcworkspace' using Xcode  
5. Run app

---

## Demo
![](/Preview/preview.gif)

---

## Images used

Annotation image can be found here (https://pngtree.com/element/down?id=NTA5NzQzMQ==&type=1&time=1613453305&token=NTFkNzliN2FjZjY4MWVmMjgxZTIyZjJiMDM1NmIxNDA=)
